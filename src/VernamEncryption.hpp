#ifndef VERNAMENCRYPTION_HPP
#define VERNAMENCRYPTION_HPP

#include "ASymetricEncryption.hpp"
#include "Raise.hpp"
module_new_raise(BadMagicNumberRaise)

#define VERNAM_MN "RBNV"
class VernamEncryption : public ASymetricEncryption
{
public:
  VernamEncryption();
  ~VernamEncryption();

  virtual bool encrypt();
  virtual bool decrypt();
  virtual bool hookPreEncryption(QByteArray & in, QByteArray & key, QByteArray &out);
  virtual bool hookPostEncryption(QByteArray & in, QByteArray & key, QByteArray &out);
  virtual bool hookPreDecryption(QByteArray & in, QByteArray & key, QByteArray &out);
  virtual bool hookPostDecryption(QByteArray & in, QByteArray & key, QByteArray &out);

protected:
  bool vernamAlgorithm(QByteArray & in, QByteArray & key, QByteArray &out);
};

#endif // VERNAMENCRYPTION_HPP
