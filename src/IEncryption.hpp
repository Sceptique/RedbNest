#ifndef IENCRYPTION_HPP
#define IENCRYPTION_HPP

#include <QString>
#include <QIODevice>

class IEncryption
{
public:
  virtual ~IEncryption() {}
  virtual bool encrypt() = 0;
  virtual bool decrypt() = 0;

  virtual bool setKeyStream(QIODevice *) = 0;
  virtual bool setDataStream(QIODevice *) = 0;
  virtual bool setOutStream(QIODevice *) = 0;
  virtual bool openKeyFile(QString &) = 0;
  virtual bool openDataFile(QString &) = 0;
  virtual bool openOutFile(QString &) = 0;
  virtual QIODevice* getKeyStream() const = 0;
  virtual QIODevice* getDataStream() const = 0;
  virtual QIODevice* getOutStream() const = 0;
  virtual bool checkStreams(bool safe = true) const = 0;
};

#endif // IENCRYPTION_HPP
