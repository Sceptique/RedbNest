#ifndef RAISE_H_
# define RAISE_H_

# include <exception>
# include <QString>

class StandardRaise : public std::exception {
public:
  StandardRaise(QString const & name,
                QString const & str,
                quint64 const line=0,
                QString const & file = "") :
    _name(name), _str(str), _line(line), _file(file) {
  }
  virtual ~StandardRaise() throw() {
  }
  virtual const char *what() const throw() {
    return (_name + " : " + _str + " at <" + _file + ":" + QString::number(_line) + ">").toUtf8().data();
  }
  void setLine(quint64 line) { _line = line; }
  void setFile(QString const & file) { _file = file; }

protected:
  QString _name;
  QString _str;
  quint64 _line;
  QString _file;
};

# define module_new_raise_from(name, base)				\
  class name : public base {						\
  public: name(QString const & name,				        \
               QString const & str,					\
               quint64 const line=0,					\
               QString const & file = "") :				\
    base(#name, str, line, file) {}					\
    virtual ~name() throw() {}						\
  };

# define module_new_raise(name) module_new_raise_from(name, StandardRaise)

# define raise(errtype, why) throw errtype(#errtype, why, __LINE__, __FILE__)

#endif /* !RAISE_H_ */
