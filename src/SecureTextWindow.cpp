#include "SecureTextWindow.hpp"
#include "ui_SecureTextWindow.h"

SecureTextWindow::SecureTextWindow(QWidget *parent) :
  QWidget(parent),
  ui(new Ui::SecureTextWindow)
{
  ui->setupUi(this);
}

SecureTextWindow::~SecureTextWindow()
{
  delete ui;
}
