#include <QCryptographicHash>
#include "SecureTextService.hpp"
#include "Raise.hpp"

module_new_raise(SecureTextServiceError)
module_new_raise_from(SecureTextServiceLockedError, SecureTextServiceError)

SecureTextService::SecureTextService() {
}

SecureTextService::~SecureTextService() {
}

bool SecureTextService::unlock(QString const & password) {
  QCryptographicHash hash(QCryptographicHash::Sha3_512);
  _filenameBase = hash.result();
  _password = password;
  _configuration.setContainerName(_filenameBase);
  if (_configuration.valid()) {
    loadKey();
    return true;
  }
  else {
    lock();
    return false;
  }
}

bool SecureTextService::lock(void) {
  _password = "";
  _filenameBase = "";
  _configuration.setContainerName("");
  return true;
}

bool SecureTextService::locked(void) const {
  return _password.isEmpty();
}

void SecureTextService::mustBeUnlocked() const {
  if (locked())
    raise(SecureTextServiceLockedError, "The ST Service is locked");
}

QString SecureTextService::getText() const {
  mustBeUnlocked();
  return "";
}

quint64 SecureTextService::getTextSize() const {
  mustBeUnlocked();
  return 0;
}

quint32 SecureTextService::getSeed() const {
  // hash from _salt + _passwd
  return 0;
}

bool SecureTextService::setText(QString const &) {
  mustBeUnlocked();
  return false;
}

bool SecureTextService::addText(const QString &) {
  mustBeUnlocked();
  return false;
}

bool SecureTextService::newSalt(void) {
  return false;
}

bool SecureTextService::loadSalt(void) {
  if (not _configuration.valid())
    return false;
  _salt = _configuration.getSalt();
  return true;
}

bool SecureTextService::loadKey(void) {
  loadSalt();
  if (_key != NULL)
    delete _key;
  _key = new SecureTextKey(getSeed(), getTextSize());
  return true;
}
