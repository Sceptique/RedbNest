#include <QFile>
#include "AGenericEncryption.hpp"
#include "Raise.hpp"
#include "QDebug"

module_new_raise(StreamRaise)
module_new_raise_from(StreamNotSetRaise, StreamRaise)
module_new_raise_from(StreamNotReadableRaise, StreamRaise)
module_new_raise_from(StreamNotWritableRaise, StreamRaise)

AGenericEncryption::AGenericEncryption() {
  _key_stream = NULL;
  _data_stream = NULL;
}

AGenericEncryption::~AGenericEncryption() {
  //if (_key_stream)
  //  _key_stream->close();
  //if (_data_stream)
  //  _data_stream->close();
  //if (_out_stream)
  //  _out_stream->close();
  //  delete _key_stream;
  //  delete _data_stream;
  //  delete _out_stream;
}

bool AGenericEncryption::setKeyStream(QIODevice * s) {
//  _key_stream = new QIODevice(s);
  _key_stream = s;
  return true;
}

bool AGenericEncryption::setDataStream(QIODevice * s) {
//  _data_stream = new QIODevice(s);
  _data_stream = s;
  return true;
}

bool AGenericEncryption::setOutStream(QIODevice *s) {
//  _data_stream = new QIODevice(s);
  _out_stream = s;
  return true;
}

bool AGenericEncryption::openKeyFile(QString &str) {
  _key_stream = new QFile(str);
  return true;
}

bool AGenericEncryption::openDataFile(QString &str) {
  _data_stream = new QFile(str);
  return true;
}

bool AGenericEncryption::openOutFile(QString &str) {
  _out_stream = new QFile(str);
  return true;
}

QIODevice* AGenericEncryption::getKeyStream() const {
  return _key_stream;
}

QIODevice* AGenericEncryption::getDataStream() const {
  return _data_stream;
}

QIODevice* AGenericEncryption::getOutStream() const {
  return _out_stream;
}

bool AGenericEncryption::checkStreams(bool safe) const {
  try {
    if (not _key_stream or not _data_stream or not _out_stream) // are set
      raise(StreamNotSetRaise, "Key, Data, and Out Streams must be set.");
    if (not _key_stream->isReadable()) // are readable
      raise(StreamNotReadableRaise, "Key Stream is not readable.");
    if (not _data_stream->isReadable()) // are readable
      raise(StreamNotReadableRaise, "Data Stream is not readable.");
    if (not _out_stream->isWritable()) // is writable
      raise(StreamNotWritableRaise, "Out Stream is not writable.");
  }
  catch (StandardRaise const & e) {
    if (safe) {
#ifndef PRODUCTION
      qDebug() << e.what();
#endif
      return false;
      }
    else
      throw e;
  }
  return true;
}
