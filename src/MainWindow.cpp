#include "MainWindow.hpp"
#include "ui_MainWindow.h"

#include <QFileDialog>
#include "IEncryption.hpp"
#include "VernamEncryption.hpp"
#include "Raise.hpp"

MainWindow::MainWindow(QWidget *parent) :
  QMainWindow(parent),
  ui(new Ui::MainWindow) {
  ui->setupUi(this);
  ui->encryptionRadioButton->setChecked(true);
  ui->dataFilePath->setEnabled(true);
  ui->dataFileButton->setEnabled(true);
  ui->keyFilePath->setEnabled(false);
  ui->keyFileButton->setEnabled(false);
  ui->outFilePath->setEnabled(false);
  ui->outFileButton->setEnabled(false);
  ui->encryptionButton->setEnabled(false);
  //connect(ui->encryptionRadioButton, SIGNAL(textEdited(QString)), this, SLOT(updateConfigFormInfos()));
  //connect(ui->decryptionRadioButton, SIGNAL(textEdited(QString)), this, SLOT(updateConfigFormInfos()));
  connect(ui->dataFilePath, SIGNAL(textChanged(QString)), this, SLOT(updateConfigFormInfos()));
  connect(ui->keyFilePath, SIGNAL(textChanged(QString)), this, SLOT(updateConfigFormInfos()));
  connect(ui->outFilePath, SIGNAL(textChanged(QString)), this, SLOT(updateConfigFormInfos()));
  connect(ui->encryptionButton, SIGNAL(clicked()), this, SLOT(runEncryption()));
  connect(ui->dataFileButton, SIGNAL(clicked()), this, SLOT(openDataFileWizard()));
  connect(ui->keyFileButton, SIGNAL(clicked()), this, SLOT(openKeyFileWizard()));
  connect(ui->outFileButton, SIGNAL(clicked()), this, SLOT(openOutFileWizard()));
}

MainWindow::~MainWindow() {
  delete ui;
}

#include <QDebug>
void MainWindow::updateConfigFormInfos() {
  qDebug() << "UPDATE";
  _dataPath = ui->dataFilePath->text();
  _keyPath = ui->keyFilePath->text();
  _outPath = ui->outFilePath->text();
  _keyOk = _outOk = _encryptOk = false;
  // TODO : check if not dir
  if (QFile::exists(_dataPath)) {
      _keyOk = true;
      if (QFile::exists(_keyPath)){
          _outOk = true;
          // TODO : check if valid file name
          if (_outPath.size() > 0) {
              _encryptOk = true;
            }
        }
    }
  updateConfigForm();
}

void MainWindow::updateConfigForm() {
  updateConfigFormKey();
  updateConfigFormOut();
  updateConfigFormEncrypt();
}

void MainWindow::updateConfigFormKey() {
  if (_keyOk) {
      ui->keyFileButton->setEnabled(true);
      ui->keyFilePath->setEnabled(true);
    }
  else {
      ui->keyFileButton->setEnabled(false);
      ui->keyFilePath->setEnabled(false);
    }
}

void MainWindow::updateConfigFormOut() {
  if (_outOk) {
      ui->outFileButton->setEnabled(true);
      ui->outFilePath->setEnabled(true);
    }
  else {
      ui->outFileButton->setEnabled(false);
      ui->outFilePath->setEnabled(false);
    }
}

void MainWindow::updateConfigFormEncrypt() {
  if (_encryptOk) {
      ui->encryptionButton->setEnabled(true);
    }
  else {
      ui->encryptionButton->setEnabled(false);
    }
}

void MainWindow::runEncryption() const {
  IEncryption *engine = new VernamEncryption();
  QFile *in, *k, *out;
  in = new QFile(_dataPath);
  k = new QFile(_keyPath);
  out = new QFile(_outPath);
  in->open(QIODevice::ReadOnly);
  k->open(QIODevice::ReadOnly);
  out->open(QIODevice::WriteOnly);
  engine ->setDataStream(in);
  engine ->setKeyStream(k);
  engine ->setOutStream(out);
  try {
    if (ui->encryptionRadioButton->isChecked())
      engine ->encrypt();
    else if (ui->decryptionRadioButton->isChecked())
      engine ->decrypt();
  }
  catch (StandardRaise const & e) {
    qDebug() << "Standard error : " << e.what();
  }
  catch (std::exception const & e) {
    qDebug() << "Unknown error (please report to the team) : " << e.what();
  }
  delete engine ;
  delete in;
  delete k;
  delete out;
}

void MainWindow::openDataFileWizard(void) {
  ui->dataFilePath->setText(QFileDialog::getOpenFileName(this, tr("Select a file", "Select a file"), "", "All files (*)"));
}

void MainWindow::openKeyFileWizard(void) {
  ui->keyFilePath->setText(QFileDialog::getOpenFileName(this, tr("Select a key file", "Select a key file"), "", "All files (*)"));
}

void MainWindow::openOutFileWizard(void) {
  ui->dataFilePath->setText(QFileDialog::getSaveFileName(this, tr("Save a file", "Save a file"), ""));
}
