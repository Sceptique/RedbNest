#ifndef AGENERICENCRYPTION_HPP
#define AGENERICENCRYPTION_HPP

#include "IEncryption.hpp"

class AGenericEncryption : public IEncryption
{
public:
  AGenericEncryption();
  virtual ~AGenericEncryption();

  virtual bool encrypt() = 0;
  virtual bool decrypt() = 0;

  virtual bool setKeyStream(QIODevice *);
  virtual bool setDataStream(QIODevice *);
  virtual bool setOutStream(QIODevice *);
  virtual bool openKeyFile(QString &);
  virtual bool openDataFile(QString &);
  virtual bool openOutFile(QString &);
  virtual QIODevice* getKeyStream() const;
  virtual QIODevice* getDataStream() const;
  virtual QIODevice* getOutStream() const;
  virtual bool checkStreams(bool safe = true) const;

protected:
  QIODevice *_key_stream;
  QIODevice *_data_stream;
  QIODevice *_out_stream;
};

#endif // AGENERICENCRYPTION_HPP
