#ifndef SECURETEXTSERVICE_HPP
#define SECURETEXTSERVICE_HPP

#include <QString>
#include "IEncryption.hpp"
#include "SecureTextConfiguration.hpp"
#include "SecureTextKey.hpp"

class SecureTextService
{
public:
  SecureTextService();
  ~SecureTextService();

  /*
   * The function will try to unlock an encrypted file
   * The filename of the container is based on the Sha3_512 hash of the password
   * The container is named like HASH.enc
   * The salt associated is named like HASH.salt
   * The key is generated with a seed based on the HASH of salt + passwd
   */
  bool unlock(QString const & password);
  bool lock(void);
  QString getText() const;
  quint64 getTextSize() const;
  quint32 getSeed() const;
  bool setText(QString const &); // newSalt + save new text
  bool addText(QString const &); // getText() + text -> setText()
  bool locked(void) const;
  void mustBeUnlocked() const;

private:
  bool newSalt(void); // TODO: generate random salt 32bits
  bool loadSalt(void);
  bool loadKey(void); // TODO: regen _key

private:
  SecureTextConfiguration _configuration;
  QIODevice * _key;
  IEncryption * _engine;
  QString _filenameBase;
  QString _password;
  quint64 _salt;
};

#endif // SECURETEXTSERVICE_HPP
