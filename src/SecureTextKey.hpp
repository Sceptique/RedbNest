#pragma once

#include <QFile>
#include <QByteArray>

class SecureTextKey : public QFile
{
public:
  // TODO: each change of an attribute should run the srand()
  //and store the key
  SecureTextKey(quint32 seed, quint32 size);
  quint32 getSeed() const;
  quint32 getSize() const;
  void setSeed(quint32 i);
  void setSize(quint32 i);
  void load();//TODO
  QByteArray readAll(); //TODO

private:
  quint32 _seed;
  quint32 _size;
};
