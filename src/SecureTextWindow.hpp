#ifndef SECURETEXTWINDOW_HPP
#define SECURETEXTWINDOW_HPP

#include <QWidget>

namespace Ui {
  class SecureTextWindow;
}

class SecureTextWindow : public QWidget
{
  Q_OBJECT

public:
  explicit SecureTextWindow(QWidget *parent = 0);
  ~SecureTextWindow();

private:
  Ui::SecureTextWindow *ui;
};

#endif // SECURETEXTWINDOW_HPP
