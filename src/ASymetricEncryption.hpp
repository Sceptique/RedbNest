#ifndef ASYMETRICENCRYPTION_HPP
#define ASYMETRICENCRYPTION_HPP

#include "AGenericEncryption.hpp"

class ASymetricEncryption : public AGenericEncryption
{
public:
  ASymetricEncryption();
  ~ASymetricEncryption();

  virtual bool encrypt() = 0;
  virtual bool decrypt() = 0;
};

#endif // ASYMETRICENCRYPTION_HPP
