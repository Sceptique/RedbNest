#-------------------------------------------------
#
# Project created by QtCreator 2015-03-03T02:04:32
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = RedbNest
TEMPLATE = app


SOURCES += main.cpp \
    MainWindow.cpp \
    AGenericEncryption.cpp \
    ASymetricEncryption.cpp \
    VernamEncryption.cpp \
    SecureTextConfiguration.cpp \
    SecureTextService.cpp \
    SecureTextWindow.cpp \
    SecureTextKey.cpp

HEADERS  += \
    Raise.hpp \
    MainWindow.hpp \
    IEncryption.hpp \
    AGenericEncryption.hpp \
    ASymetricEncryption.hpp \
    VernamEncryption.hpp \
    helper_generic.hpp \
    SecureTextConfiguration.hpp \
    SecureTextService.hpp \
    SecureTextWindow.hpp \
    SecureTextKey.hpp

FORMS    += \
    MainWindow.ui \
    SecureTextWindow.ui
