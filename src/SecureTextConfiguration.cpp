#include <QFile>
#include "SecureTextConfiguration.hpp"

SecureTextConfiguration::SecureTextConfiguration() {
}

SecureTextConfiguration::~SecureTextConfiguration() {
}

QString const & SecureTextConfiguration::getEngineTypeName() const {
  return _engineTypeName;
}

QString const & SecureTextConfiguration::getContainerFileName() const {
  return _containerFileName;
}

QString const & SecureTextConfiguration::getSaltFileName() const {
  return _saltFileName;
}

QString const & SecureTextConfiguration::getContainerName() const {
  return _containerName;
}

void SecureTextConfiguration::setEngineTypeName(QString const & s) {
  _engineTypeName = s;
}

void SecureTextConfiguration::setContainerName(QString const & s) {
  _containerName = s;
  _containerFileName = STC_BASEDIR + s + STC_CONTAINER_EXT;
  _saltFileName = STC_BASEDIR + s + STC_SALT_EXT;
}

quint32 SecureTextConfiguration::getSalt() const {
  QFile saltFile(_saltFileName);
  saltFile.open(QIODevice::ReadOnly);
  quint32 salt = saltFile.readAll().toUInt();
  saltFile.close();
  return salt;
}

QIODevice * SecureTextConfiguration::getEncryptedContainer() const {
  QIODevice * contFile = new QFile(_containerFileName);
  contFile->open(QIODevice::ReadOnly);
  return contFile;
}

bool SecureTextConfiguration::valid() const {
  QFile saltFile(_saltFileName);
  QFile contFile(_containerFileName);
  return saltFile.exists() and contFile.exists();
}
