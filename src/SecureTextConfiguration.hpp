#ifndef SECURETEXTCONFIGURATION_HPP
#define SECURETEXTCONFIGURATION_HPP

# include <QString>
# include <QIODevice>

# define STC_DEFAULT_ENGINE "Vernam"
# define STC_BASEDIR ".config/RedbNest/"
# define STC_CONTAINER_EXT ".enc"
# define STC_SALT_EXT ".salt"

class SecureTextConfiguration
{
public:
  SecureTextConfiguration();
  ~SecureTextConfiguration();

  QString const & getEngineTypeName() const;
  QString const & getContainerFileName() const;
  QString const & getSaltFileName() const;
  QString const & getContainerName() const;
  void setEngineTypeName(QString const & s);
  void setContainerName(QString const & s);
  quint32 getSalt() const;
  QIODevice * getEncryptedContainer() const;
  bool valid() const;

private:
  QString _containerName;
  QString _engineTypeName;
  QString _containerFileName;
  QString _saltFileName;
};

#endif // SECURETEXTCONFIGURATION_HPP
