#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QString>
#include <QFile>

namespace Ui {
  class MainWindow;
}

class MainWindow : public QMainWindow
{
  Q_OBJECT

public:
  explicit MainWindow(QWidget *parent = 0);
  ~MainWindow();

public slots:
  void updateConfigFormInfos(void);
  void runEncryption(void) const;
  void openDataFileWizard(void);
  void openKeyFileWizard(void);
  void openOutFileWizard(void);

public:
  void updateConfigForm(void);
  void updateConfigFormKey(void);
  void updateConfigFormOut(void);
  void updateConfigFormEncrypt(void);

private:
  Ui::MainWindow *ui;
  QString _dataPath;
  QString _keyPath;
  QString _outPath;
  bool _keyOk;
  bool _outOk;
  bool _encryptOk;
};

#endif // MAINWINDOW_H
