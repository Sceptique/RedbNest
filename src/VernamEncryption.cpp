#include <QByteArray>
#include <QTime>
#include "VernamEncryption.hpp"
#include "helper_generic.hpp"
#include "Raise.hpp"

/*
 * Data encapsulation
 *
 * The data are encapsuled like the following structure
 * magic number: 4 BYTES: magic number
 * size segment: 8 BYTES: size of the compressed data segment
 * offset size segment: 2 BYTES: size of the offset (B) segment
 * random offset segment (B): N BYTES: random
 * compressed data segment: N BYTES: compressed data
 * random offset segment (A): N BYTES: random
 */
VernamEncryption::VernamEncryption() {
  qsrand(QTime::currentTime().msec());
}

VernamEncryption::~VernamEncryption() {

}

bool VernamEncryption::vernamAlgorithm(QByteArray &in, QByteArray &key, QByteArray &out) {
  QByteArray::iterator idx = in.begin();
  QByteArray::iterator idk = key.begin();
  while (idx != in.end()) {
      if (idk == key.end())
        idk = key.begin();
      // qDebug() << (int)*idx << " ^ " << (int)*idk << " = " <<  (int)(*idx ^ *idk);
      out.push_back(*idx ^ *idk);
      ++idk;
      ++idx;
    }
  return true;
}

#define DEBUG_STREAM_SAFE true
bool VernamEncryption::encrypt() {
  if (not checkStreams(DEBUG_STREAM_SAFE))
    return false;
  QByteArray in, key, out;
  return (hookPreEncryption(in, key, out) and
          vernamAlgorithm(in, key, out) and
          hookPostEncryption(in, key, out));
}

bool VernamEncryption::decrypt() {
  if (not checkStreams(DEBUG_STREAM_SAFE))
    return false;
  QByteArray in, key, out;
  return (hookPreDecryption(in, key, out) and
          vernamAlgorithm(in, key, out) and
          hookPostDecryption(in, key, out));
}

bool VernamEncryption::hookPreEncryption(QByteArray & in,
                                         QByteArray & key,
                                         QByteArray &) {
  in = qCompress(_data_stream->readAll());
  quint64 const size = in.size();
  key = _key_stream->readAll();

  // OFFSET A
  for (quint16 i = qrand() % 0xffff; i > 0; i--)
    in.push_back(qrand() % 0xff);

  // OFFSET B
  quint16 const offset = qrand() % 0xffff;
  for (quint16 i = 0; i < offset; i++)
    in.push_front(qrand() % 0xff);

  // SIZE
  in.push_front((quint8)(size >> 0));
  in.push_front((quint8)(size >> 8));
  in.push_front((quint8)(size >> 16));
  in.push_front((quint8)(size >> 24));
  in.push_front((quint8)(size >> 32));
  in.push_front((quint8)(size >> 40));
  in.push_front((quint8)(size >> 48));
  in.push_front((quint8)(size >> 56));

  // OFFSET B SIZE
  in.push_front((quint8)(offset >> 0));
  in.push_front((quint8)(offset >> 8));

  return true;
}

bool VernamEncryption::hookPostEncryption(QByteArray &,
                                          QByteArray &,
                                          QByteArray & out) {
  _out_stream->write(VERNAM_MN);
  _out_stream->write(out.data(), out.size());
  _out_stream->reset();
  return true;
}

#include <QDebug>
bool VernamEncryption::hookPreDecryption(QByteArray & in,
                                         QByteArray & key,
                                         QByteArray &) {
  in = _data_stream->readAll();
  if (in.left(sizeof(VERNAM_MN) - 1) != VERNAM_MN)
    raise(BadMagicNumberRaise, "This is not a valid Vernam container.");
  else
    in.remove(0, sizeof(VERNAM_MN) - 1);
  key = _key_stream->readAll();
  return true;
}

bool VernamEncryption::hookPostDecryption(QByteArray &,
                                          QByteArray &,
                                          QByteArray & out) {
  // OFFSET B SIZE
  quint16 const offset = ((quint8)(out[0]) << 8) +
      ((quint8)(out[1]) << 0);
  out.remove(0, 2);

  // SIZE
  quint64 const size = ((quint64)(out[0]) << 56) +
      ((quint64)(out[1]) << 48) +
      ((quint64)(out[2]) << 40) +
      ((quint64)(out[3]) << 32) +
      ((quint64)(out[4]) << 24) +
      ((quint64)(out[5]) << 16) +
      ((quint64)(out[6]) << 8) +
      ((quint64)(out[7]) << 0);

  out.remove(0, 8); // Remove the size segment
  out.remove(0, offset); // Remove the random segment (B)
  out.remove(size, out.size() - size); // Remove the random segment (A)

  out = qUncompress(out); // Uncompress the real data segment
  _out_stream->write(out.data(), out.size());
  _out_stream->reset();
  return true;
}
