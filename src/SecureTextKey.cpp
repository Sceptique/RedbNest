#include "SecureTextKey.hpp"

SecureTextKey::SecureTextKey(quint32 seed, quint32 size)
  : _seed(seed),
    _size(size) {
}

quint32 SecureTextKey::getSeed() const {
  return _seed;
}

quint32 SecureTextKey::getSize() const {
  return _size;
}

void SecureTextKey::setSeed(quint32 i) {
  _seed = i;
}

void SecureTextKey::setSize(quint32 i) {
  _size = i;
}

void SecureTextKey::load() {
  qsrand(_seed);
}

QByteArray SecureTextKey::readAll() {

}

