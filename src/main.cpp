#include "MainWindow.hpp"
#include <QApplication>

#include "AGenericEncryption.hpp"
#include "VernamEncryption.hpp"
#include <QFile>
#include <QFileDevice>
#include <QIODevice>
#include <QDebug>

int test(int ac, char *av[]) {
  QString key, in, out;
  out = (ac < 4) ? "out" : av[3] ;
  key = (ac < 3) ? "key" : av[2] ;
  in = (ac < 2) ? "in" : av[1] ;

  qDebug() << "Files : IN(" << in << ") KEY(" << key << ") OUT(" << out << ")";
  IEncryption *engine = new VernamEncryption();
  QIODevice *iokey, *ioin, *ioout;
  ioin = new QFile(in);
  ioin->open(QIODevice::ReadOnly);
  iokey = new QFile(key);
  iokey->open(QIODevice::ReadOnly);
  ioout = new QFile(out);
  ioout->open(QIODevice::WriteOnly);
  engine->setDataStream(ioin);
  engine->setKeyStream(iokey);
  engine->setOutStream(ioout);
  if (ac < 4 or QString("encrypt") == av[4]) {
      qDebug() << "Encryption... (" << ioin->size() << ")";
      qDebug() << engine->encrypt();
    }
  else if (QString("decrypt") == av[4]) {
      qDebug() << "Decryption... (" << ioin->size() << ")";
      qDebug() << engine->decrypt();
    }
  else
    qDebug() << "Select a method as last argument (accepted : 'encrypt' or 'decrypt')";
  delete iokey;
  delete ioin;
  delete ioout;
  delete engine;
  return 0;
}

int main(int argc, char *argv[]) {
  if (argc > 1)
    return test(argc, argv);

  QApplication a(argc, argv);
  MainWindow w;
  w.show();

  return a.exec();
}
